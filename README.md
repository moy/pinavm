Welcome to the project PinaVM. PinaVM is a prototype of a
[SystemC](http://systemc.org) front-end based on
[LLVM](http://llvm.org). It is somehow the successor of
[Pinapa](http://greensocs.sourceforge.net/pinapa/), although the
projects do not share any code.

PinaVM is still a prototype, and doesn't have very good user
documentation as of now. However, interested users can read:

-   [PinaVM: a SystemC Front-End Based on an Executable Intermediate
    Representation](http://www-verimag.imag.fr/details.html?pub_id=pinavm-emsoft),
    published at EMSOFT 2010 (see also [the associated technical report](http://www-verimag.imag.fr/Technical-Reports,264.html?&number=TR-2010-8))
-   [A Theoretical and Experimental Review of SystemC
    Front-ends](http://www-verimag.imag.fr/details.html?pub_id=review-sc-fe),
    published at FDL 2010 (see also
    [the associated technical report](http://www-verimag.imag.fr/Technical-Reports,264.html>?&number=TR-2010-4))
-   [Efficient Encoding of SystemC/TLM in
    Promela](http://www-verimag.imag.fr/details.html?pub_id=MARQUET:2011:HAL-00557515:1),
    published at DATICS-IMECS 2011 (see also
    [Research report about one of the back-ends of PinaVM the associated
    technical report](http://www-verimag.imag.fr/Technical-Reports,264.html>?&number=TR-2010-7))

PinaVM can be downloaded from https://gitlab.com/moy/pinavm.
It is primarily developed on Linux, but works on Mac OS X too.
